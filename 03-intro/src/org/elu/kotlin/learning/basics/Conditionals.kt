package org.elu.kotlin.learning.basics

fun main(args: Array<String>) {
    var myString = "not empty"
    if (myString != "") {
        println(myString)
    } else {
        println("Is empty")
    }

    val result = if (myString != "") {
        "Not empty"
    } else {
        "Empty"
    }
    println("result: " + result)

    when (result) {
        "Not empty" -> println("Excellent")
        "Empty" -> println("Not Excellent")
    }

    myString = "Value"
    val whenValue = when (myString) {
        "Value" -> {
            println("It's a value")
            println("Another statement")
            "Returning form first when case"
        }
        is String -> "String"
        else -> "It came to this?"
    }
    println("when value: " + whenValue)
}