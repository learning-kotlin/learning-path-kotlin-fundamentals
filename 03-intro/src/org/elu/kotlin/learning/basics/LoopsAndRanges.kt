package org.elu.kotlin.learning.basics

fun main(args: Array<String>) {
    for (a: Int in 1..10) {
        println(a)
    }

    for (a in 1..10) {
        println(a)
    }

    val numbers = 1..10
    for (a in numbers) {
        println(a)
    }

    for (a in 10 downTo 1) {
        println(a)
    }

    for (i in 10 downTo 1 step 2) {
        println(i)
    }

    val capitals = listOf("London", "Paris", "Rome", "Madrid")
    for (capital in capitals) {
        println(capital)
    }

    var y = 10
    while (y > 0) {
        println(y)
        y--
    }

    var x = 10
    do {
        println(x)
        x--
    } while (x > 0)

    loop@ for (i in 2..20) {
        for (j in 1..10) {
            println("i = $i, j = $j")
            if (j % i == 0) {
                break@loop
            }
        }
    }
}