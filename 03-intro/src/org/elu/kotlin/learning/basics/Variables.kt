package org.elu.kotlin.learning.basics

fun main(args: Array<String>) {
    // mutable variables
    var streetNumber: Int
    var streetName: String = "Banhofstrasse"
    // type is inferred
    var otherStreetName = "Kotikatu"

    streetNumber = 10
    streetNumber = 12

    // immutable variable
    val zip = "00900"

    // different types
    val myLong = 10L
    val myFloat = 100F
    val myHex = 0x0F
    val myBinary = 0xb01

    // no explicit type conversion
    val myInt = 10
    val myLongAgain = myInt.toLong()

    // String
    val myChar = 'A'
    val myString = "My String"

    val escapeCharacters = "A new line \n "

    val rawString = "Hello" +
            "This is second line" +
            "A third line"

    val multipleLines = """
        This is a string
        And this is another line
        """

    println(multipleLines)

    // string interpolation
    val years = 10
    val message = "A decade is $years"
    println(message)

    val name = "Janna"
    val anotherMessage = "Length of name is ${name.length}"
    println(anotherMessage)
}
